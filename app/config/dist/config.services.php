<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

use Monolog\Logger;

/**
 * Services Configuration - Any 3rd Party Configurations
 */
return [
    'services'    => [
        'mail'    => [
            'host' => getenv('ENV_MAIL_HOST') ?: 'mail.test',
            'port' => getenv('ENV_MAIL_PORT') ?: 25,
            'from' => 'do-not-reply@localhost.test'
        ],
        'logging' => [
            'to'          => 'info@localhost.test',
            'env_levels'  => [
                'default'    => Logger::WARNING,
                'production' => Logger::ERROR
            ]
        ],
        'analytics' => [
            //Google Analytics
            'id' => null
        ],
        'cache' => [
            'serviceCache' => [
                'adapter'  => 'none',
                'index'    => 'serviceCache',
                'frontend' => [
                    'adapter' => 'data'
                ]
            ]
        ],
        'middleware' => []
    ]
];
