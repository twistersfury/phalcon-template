<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Defines;

/*
 * Debug Stuff
 */
define('TF_DEBUG_DISABLED', 0);
define('TF_DEBUG_ENABLED', 1);
define('TF_DEBUG_PHALCON', 2);
define('TF_DEBUG_SQL', 4);

/*
 * Path Stuff
 */
define('DS', DIRECTORY_SEPARATOR);


/*
 *  Base Config Stuff
 */

define(
    'TF_LOADER_NAMESPACES',
    [
        'TwistersFury\\Phalcon\\Template' => realpath(__DIR__ . '/../../modules') . '/'
    ]
);

define(
    'TF_LOADER_DIRECTORIES',
    []
);

define(
    'TF_LOADER_FILES',
    []
);
