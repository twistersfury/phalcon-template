<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Merits;

define('APPLICATION_PATH', realpath(__DIR__ . '/../..'));
define('APPLICATION_ENV', getenv('ENV_APPLICATION_ENV') ?: 'production');
define('DOC_ROOT', realpath(APPLICATION_PATH . '/../public'));

(function () {
    $debugMode = TF_DEBUG_DISABLED;

    if (getenv('ENV_DEBUG_MODE')) {
        $debugMode = getenv('ENV_DEBUG_MODE');
    } elseif (APPLICATION_ENV === 'development') {
        $debugMode = TF_DEBUG_ENABLED | TF_DEBUG_PHALCON | TF_DEBUG_SQL;
    }

    define('TF_DEBUG', $debugMode);
})();
