<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

/**
 * Any Providers Get Registered Here
 */
return [
    'providers' => [
        TwistersFury\Phalcon\Shared\Di\ServiceProvider\Database::class,
        TwistersFury\Phalcon\Shared\Di\ServiceProvider\Cache::class
    ],
    'mvc_providers' => [
        TwistersFury\Phalcon\Shared\Di\ServiceProvider\Mvc::class,
    ],
    //    'cli_providers' => []
];
