<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

use TwistersFury\Phalcon\Template\Site\Mvc\Module as SiteModule;

/**
 * Any Multi-Modules Get Configured Here
 */
return [
    'modules' => [
        'site' => [
            'className' => SiteModule::class,
            'path'      => APPLICATION_PATH . '/modules/Site/Mvc/Module.php'
        ]
    ]
];
