<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Phalcon\Template\Site\Mvc;

use TwistersFury\Phalcon\Shared\Mvc\AbstractModule;

class Module extends AbstractModule
{
}
