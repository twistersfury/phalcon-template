<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Phalcon\Template\Site\Mvc\Controller;

use TwistersFury\Phalcon\Shared\Mvc\Controller\AbstractController;

/**
 * Class GeneralController
 *
 * @package TwistersFury\Phalcon\Template\Site\Mvc\Controller
 * @property \Phalcon\Mvc\View $view
 */
class GeneralController extends AbstractController
{
    public function indexAction()
    {
        $this->view->setVar('hello', 'world');
    }
}
