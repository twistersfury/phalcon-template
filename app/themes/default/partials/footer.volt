<footer>
    <i>footer.volt</i>
</footer>

{{ assets.outputJs(config.system.theme ~ '-external') }}
{{ assets.outputJs(config.system.theme ~ '-internal') }}

{% if config.environment === 'production' %}
    {{  partial('google_analytics') }}
{% endif %}
