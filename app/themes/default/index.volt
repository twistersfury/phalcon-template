<!DOCTYPE html>
<html>
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    {{ get_title() }}

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Porto - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url.getStatic('images/favicon.ico') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ url.getStatic('img/apple-touch-icon-alt.png') }}">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-title" content="Merit Tracker">

    <!-- Phalcon Css -->
    {{ assets.outputCss(config.system.theme ~ '-external') }}
    {{ assets.outputCss(config.system.theme ~ '-internal') }}
</head>
<body>
    {{ partial('header') }}

    {{ get_content() }}

    {{ partial('footer') }}
</body>
</html>
