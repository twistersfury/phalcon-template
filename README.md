# Phalcon Template #
A basic bootstrap template to get started with Phalcon. 

## Installation ##
```bash
composer create-project twistersfury/phalcon-template myproject
``` 

## Template includes ##
* Docker Image
* Composer
  * [Codeception](https://codeception.com)
  * Infection
  * PHPStan
  * PHP-CS-Fixer
  * [Phalcon Shared](https://github.com/twistersfury/phalcon-shared)
* GitLab CI
* Phalcon Dev Tools Compatible Template
  * Loader
  * Bootstrap
  * Multi-Module Application
  * Locale
  * Themes
  
## Usage Notes ##
This template is shipped with a pre-built Makefile to use the GNUMake system. For example, make will automatically boot up your docker environment when you run `make test`. The common commands are as follows:

* `make test` - Run Codeception Testing
* `make clean` - Clean Up Extras
* `make wipe` - Full Clean Of Extras
* `make analyze` - Run tests and qa checks
* `make cs-fix` - Run PHP CS Fixer
* `make cs-check` - Run Fixer without fixing.
* `make stop` - Stop Docker
* `make start` - Start Docker  

## Docker/X-Debug - OSX ##
For xdebug to properly connect, either boot docker-compose using the `-f ./docker/php/docker-compose.osx.yml` file, or copy the file to `docker-compose.override.yml`
