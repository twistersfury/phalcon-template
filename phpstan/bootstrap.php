<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

//Load Normal PSR4
require_once __DIR__ . '/../vendor/twistersfury/phalcon-shared/phalcon_loader.php';

//Add Additional PHPStan PS4's
(function () {
//    $loader = new \Phalcon\Loader();
//
//    $loader->registerNamespaces(
//        [
//        ]
//    )->registerClasses(
//        [
//        ]
//    );
//
//    $loader->register(true);
})();
