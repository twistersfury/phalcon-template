<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

use Phalcon\Mvc\Application;

//Load Codeception Testing File, If Exists (Not In Production)
if (file_exists(__DIR__ . '/../c3.php') && getenv('ENV_APPLICATION_ENV') !== 'production') {
    require_once __DIR__ . '/../c3.php';
}

//Load Post From Input (Ajax Requests)
if (isset($_SERVER['CONTENT_TYPE']) && strpos($_SERVER['CONTENT_TYPE'], 'application/json') !== false) {
    $_POST    = array_merge($_POST, (array) json_decode(trim(file_get_contents('php://input') ?: ''), true));
    $_REQUEST = array_merge($_REQUEST, $_POST);
}

(function () {
    /** @var Application $phApplication */
    $phApplication = require_once __DIR__ . '/../vendor/twistersfury/phalcon-shared/phalcon_application.php';
    $phApplication->handle()->send();
})();
