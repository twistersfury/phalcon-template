######################################################
#        SYSTEM MAKE FILE                            #
######################################################
# Use this to allow quick/easy modifications and/or  #
# updates to the software. Requires make to be       #
# installed.                                         #
#                                                    #
# Note: This assumes you already have PHP setup and  #
#   properly configured in your path environment     #
######################################################
# Windows                                            #
#                                                    #
# http://gnuwin32.sourceforge.net/downlinks/make.php #
######################################################
# OS X / Linux                                       #
#                                                    #
# Likely Already Installed. Otherwise use the os's   #
# package manager (IE: apt-get, brew, macports, etc) #
######################################################

CACHE_PATH=cache
BIN_PATH=bin
NODE_MODULES=node_modules

COMPOSER_URL=https://getcomposer.org/installer
PHPSTAN_URL=https://github.com/phpstan/phpstan/releases/download/0.10.6/phpstan.phar
PHP-CS-FIXER_URL=https://cs.sensiolabs.org/download/php-cs-fixer-v2.phar

DOCKER_RUN=docker run --rm -it --network=phalcon -w /var/www -v $(shell pwd):/var/www php:xdebug

TEST_OPTIONS ?= --ext DotReporter

ifeq ($(OS),Windows_NT)
	COMPOSE_OPTIONS :=
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Darwin)
		COMPOSE_OPTIONS := -f docker/php/docker-compose.osx.yml
	else
		COMPOSE_OPTIONS :=
	endif
endif

######################################################
#                 Default Run Command                #
######################################################
.PHONY: default
default: analyse

######################################################
#               Hard File Dependencies               #
######################################################

# Primary Cache Directory
$(CACHE_PATH):
	mkdir -p $(CACHE_PATH)

# Primary Bin Directory
$(BIN_PATH):
	mkdir -p $(BIN_PATH)

# Composer Dependencies
vendor: composer.lock composer.json
	make composer

# Node
$(NODE_MODULES): package-lock.json package.json
	make node

# Phalcon Validation
$(BIN_PATH)/.phalcon: | $(BIN_PATH)
	(php -m | grep phalcon) || (echo "Phalcon Is Not Installed" && exit 1)
	touch $(BIN_PATH)/.phalcon

$(BIN_PATH)/phalcon-devtools: $(BIN_PATH)/.phalcon $(BIN_PATH)/composer | $(BIN_PATH) vendor
	composer create-project phalcon/devtools $(BIN_PATH)/phalcon-devtools

# Composer Binary
$(BIN_PATH)/composer: | $(BIN_PATH)
	([ -n "$(shell which composer)" ] && ln -s $(shell which composer) $(BIN_PATH)/composer) || ( \
		php -r "copy('$(COMPOSER_URL)', 'composer-setup.php');" && \
		php composer-setup.php && \
		php -r "unlink('composer-setup.php');" && \
		mv composer.phar $(BIN_PATH)/composer \
	)

# PhpStan Binary
$(BIN_PATH)/phpstan: | $(BIN_PATH) vendor  phpstan/bootstrap.php
	([ -n "$(shell which phpstan)" ] && ln -s $(shell which phpstan) $(BIN_PATH)/phpstan) || ( \
	    [ -a vendor/bin/phpstan ] && \
	    (ln -s $(shell pwd)/vendor/bin/phpstan $(BIN_PATH)/phpstan || (echo "Already Linked" && exit 0)) \
	) || ( \
		php -r "copy('$(PHPSTAN_URL)', '$(BIN_PATH)/phpstan');" && \
		chmod +x $(BIN_PATH)/phpstan \
	)

# Php-Cs-Fixer Binary
$(BIN_PATH)/php-cs-fixer: | $(BIN_PATH) vendor
	([ -n "$(shell which php-cs-fixer)" ] && ln -s $(shell which php-cs-fixer) $(BIN_PATH)/php-cs-fixer) || ( \
        [ -a vendor/bin/php-cs-fixer ] && \
        ln -s $(shell pwd)/vendor/bin/php-cs-fixer $(BIN_PATH)/php-cs-fixer \
    ) || ( \
		php -r "copy ('$(PHP-CS-FIXER_URL)', '$(BIN_PATH)/php-cs-fixer');" && \
		chmod +x $(BIN_PATH)/php-cs-fixer \
	)

$(BIN_PATH)/.build: $(shell find ./docker -type f -name "*")
	docker-compose -f ./docker-compose.yml $(COMPOSE_OPTIONS) build $(DC_BUILD_IMAGE)
	touch $(BIN_PATH)/.build

# Local Selenium (For Testing)
$(BIN_PATH)/.compose: $(BIN_PATH)/.phalcon $(BIN_PATH)/.docker $(BIN_PATH)/.build docker-compose.yml | $(BIN_PATH) vendor
	# Stop Any Running Docker Items
	make stop
	docker-compose -f ./docker-compose.yml $(COMPOSE_OPTIONS) up -d

	# More or less a dummy placeholder - Just an image that isn't used that doesn't exit for CID File.
	rm -f $(BIN_PATH)/.compose
	docker run --cidfile $(BIN_PATH)/.compose --rm williamyeh/dummy

	# Giving Time To Boot Up
	sleep 60

# Docker Installation
$(BIN_PATH)/.docker: | $(BIN_PATH)
	which docker || (echo "Docker Is Not Installed" && exit 1)
	touch $(BIN_PATH)/.docker

######################################################
#            Installation Dependencies               #
######################################################

.PHONY: clean wipe composer install node

install: vendor tests/_data/.env $(BIN_PATH)/phalcon-devtools $(BIN_PATH)/php-cs-fixer $(BIN_PATH)/phpstan migrate

clean: stop clear-cache
	rm -rf $(BIN_PATH)
	rm -rf vendor
	rm -rf $(NODE_MODULES)
	rm -rf tests/_data/.env
	rm -rf infection-log*.log
	rm -rf c3.php
	rm -rf html

wipe: clean
	rm -rf docker-compose.override.yml
	rm -rf composer.lock

composer: $(BIN_PATH)/composer composer.json composer.lock
	composer install

node: package.json package-lock.json
	npm install


######################################################
#                Environment Commands                #
######################################################

tests/_data/.env:
	cp tests/_data/.env.sample tests/_data/.env

######################################################
#                  Testing Commands                  #
######################################################

.PHONY: test test-unit test-functional test-acceptance

# Run All Testing
test: $(BIN_PATH)/.compose tests/_data/.env | vendor
	$(DOCKER_RUN) php $(PHP_ARGS) ./vendor/codeception/codeception/codecept run $(TEST_OPTIONS)

# Run Just The Unit Suite
test-unit: $(BIN_PATH)/.compose tests/_data/.env | vendor
	$(DOCKER_RUN) php $(PHP_ARGS) ./vendor/codeception/codeception/codecept run unit --ext DotReporter

# Run The Functional Suite
test-functional: $(BIN_PATH)/.compose tests/_data/.env | vendor
	$(DOCKER_RUN) php $(PHP_ARGS) ./vendor/codeception/codeception/codecept run functional --ext DotReporter

# Run The Acceptance Suite (Needs Full Build)
test-acceptance: $(BIN_PATH)/.compose tests/_data/.env | vendor
	$(DOCKER_RUN) php $(PHP_ARGS) ./vendor/codeception/codeception/codecept run acceptance --ext DotReporter


######################################################
#             Quality Assurance Commands             #
######################################################
.PHONY: analyse cs-check cs-fix phploc phpstan

phploc: vendor/bin/phploc
	php -d memory_limit=-1 vendor/bin/phploc --exclude vendor --exclude node_modules .

cs-check: $(BIN_PATH)/php-cs-fixer | vendor $(CACHE_PATH)
	php -d memory_limit=-1 $(BIN_PATH)/php-cs-fixer fix --verbose --dry-run --stop-on-violation --cache-file=$(CACHE_PATH)/.php_cs.cache

cs-fix: $(BIN_PATH)/php-cs-fixer | vendor $(CACHE_PATH)
	php -d memory_limit=-1 $(BIN_PATH)/php-cs-fixer fix --verbose --cache-file=$(CACHE_PATH)/.php_cs.cache

phpstan: $(BIN_PATH)/phpstan phpstan.neon.dist | vendor
	php -d memory_limit=-1 $(BIN_PATH)/phpstan analyse . $(PHPSTAN_OPTIONS) -c phpstan.neon.dist

######################################################
#               Phalcon Helper Commands              #
######################################################

.PHONY: clear-cache migrate

# Clear Project Caches
clear-cache: $(CACHE_PATH)
	find $(CACHE_PATH) ! -name .gitignore -delete

migrate: $(BIN_PATH)/phalcon-devtools
	php $(BIN_PATH)/phalcon-devtools/phalcon.php migration run --log-in-db

######################################################
#              General Helper Commands               #
######################################################
.PHONY: analyse stop start

# Run All Testing/QA
analyse: phpstan cs-check test

# Stop Docker Images
stop:
	docker-compose -f ./docker-compose.yml $(COMPOSE_OPTIONS) down || (echo 'Compose Not Running' && exit 0)
	docker stop $(shell docker ps -q) || (echo 'No Images Running' && exit 0)
	rm -f $(BIN_PATH)/.compose

start: $(BIN_PATH)/.compose

######################################################
#              Cleanup Helper Commands               #
######################################################

.PHONY: remove-default-module

remove-default-module: app/modules/Site
	rm -rf app/modules/Site
